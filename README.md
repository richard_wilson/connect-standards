# Connect Coding Standards & Parent POM

## Overview

This project is designed to hold the standard Connect coding standard configurations. This is then brought together with a parent pom for Java projects to 
extend.

[More details on the Wiki](https://digital.jaguarlandrover.com/display/DDP/Coding+Standards)

## Java Format

Connect is using the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)

A configuration file that is compatible with this is found in the root of this project: "connect-intellij-format.xml"

## Checkstyle

A [Checkstyle](http://checkstyle.sourceforge.net/) configuration file that is compatible with the Google Java Style guide is also included in this project 
and packaged up as a jar file so it can be depended upon by the parent POM. This means projects just need to inherit from the parent POM to get the config.

## PMD

A [PMD](https://pmd.github.io/) configuration file that is compatible with the Google Java Style guide and Checkstyle is also included in this project 
and packaged up as a jar file so it can be depended upon by the parent POM. This means projects just need to inherit from the parent POM to get the config.

## FindBugs

A [FindBugs](http://findbugs.sourceforge.net/) configuration is turned on by default in the parent poms. 
This means projects just need to inherit from the parent POM to get the config. It is an out of the box confgiuration of FindBugs and will fail the build by default.

## Maven Parent POM

The Maven parent POM is relatively simple and just enables Checkstyle and PMD checks. The default configuration enforces build failures on any 
Checkstyle or PMD violations, but can be configured to not enforce that whilst projects move over and fix issues.

The POM also provides the basic maven repository configuration.